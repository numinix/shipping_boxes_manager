<?php
define('MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_TITLE', 'OnTrac');
define('MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_DESCRIPTION', '<h2>OnTrac Web Services</h2><p>You will need to have registered an account with OnTrac and proper approval from OnTrac identity to use this module. Please see the README.TXT file for other requirements.</p>');
define('MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_DESCRIPTION_SOAP', '<h2>OnTrac Web Services</h2><p><span style="color:#dd0000;">Warning: SOAP Extension is not enabled. OnTrac Web Services Shipping Module will not work until SOAP is enabled. Ensure that PHP is compiled with SOAP. Speak to your hosting company if unsure.</span></p> <p>You will need to have registered an account with OnTrac and proper approval from OnTrac identity to use this module. Please see the README.TXT file for other requirements.</p>');

// eof