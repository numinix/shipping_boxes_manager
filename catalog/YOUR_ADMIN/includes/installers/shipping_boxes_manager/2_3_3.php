<?php
//Add a thickness setting to the shipping boxes manager tool 
//Edit the final package code to add thickness for each dimension

global $sniffer;

if (!$sniffer->field_exists(TABLE_SHIPPING_BOXES_MANAGER, 'thickness'))  $db->Execute("ALTER TABLE " . TABLE_SHIPPING_BOXES_MANAGER . " ADD thickness float NOT NULL default '0';");
