<?php

$db->Execute("INSERT INTO ".TABLE_CONFIGURATION." (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Enable Double Boxing if Quantity Greater Than',
'MODULE_SHIPPING_BOXES_MANAGER_DOUBLE_BOXING_QUANTITY_THRESHOLD', 0, 'Enter the value when the double boxing should be applied.', ". $configuration_group_id  .", 3, NOW(), NULL, NULL),
(NULL, 'Enable Double Boxing if Order Value Greater Than',
'MODULE_SHIPPING_BOXES_MANAGER_DOUBLE_BOXING_ORDER_VALUE_THRESHOLD', 0, 'Enter the value when the double boxing should be applied. ($)', \". $configuration_group_id  .\", 4, NOW(), NULL, NULL),
(NULL, 'Increase Box Size by X Inches When Double Boxing Enabled', 'MODULE_SHIPPING_BOXES_MANAGER_DOUBLE_BOXING_INCREASE_BY', 0, 'Enter a single unit that will be added to the length, width, and height.', \". $configuration_group_id  .\", 5, NOW(), NULL, NULL)");
