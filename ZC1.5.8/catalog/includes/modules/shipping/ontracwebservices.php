<?php
class ontracwebservices {
 var $code, $title, $description, $icon, $sort_order, $enabled;

//Class Constructor
  function ontracwebservices($always_enable=false) {
    global $order, $customer_id, $db;
    
    $this->code             = "ontracwebservices";
    $this->title            = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_TITLE;
    if (extension_loaded('soap')) {
    $this->description      = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_DESCRIPTION;
    } else {
     $this->description      = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_TEXT_DESCRIPTION_SOAP;
     }
    $this->sort_order       = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SORT_ORDER;
    //$this->icon = DIR_WS_IMAGES . 'ontrac-images/ontrac.gif';
    $this->icon = ''; 
    
    //check if it is ok to enable this shipping method
    if($always_enable == true) {
      $this->enabled = true;
    }
    else{
    $customer_country_info = zen_get_countries($order->delivery['country']['id'], true);
      if (zen_get_shipping_enabled($this->code) && extension_loaded('soap') && MODULE_SHIPPING_ONTRAC_WEB_SERVICES_STATUS == 'true' && $customer_country_info['countries_iso_code_2'] == 'US') {
        $this->enabled = true;
      }
    }
  }

  //Class Methods

  function make_package($destination_postcode, $packed_box=null, $declared_value=null) {
    global $order;
    if($packed_box == null) {
      $packed_box = $this->_find_packed_box_size();
    }

//example package string:
//ID1;90210;90210;false;0.00;false;0;5;4X3X10;S;0;0
    if(isset($_SESSION['shipping_address_is_residential']) && $_SESSION['shipping_address_is_residential']) {
      $residential = 'true';
     }
     else {
       $residential = 'false';
     }

    return array(
      'UID'=>'ID1',  //OnTrac notes: For customer use only. This value is returned in the response
      'PUZip' => MODULE_SHIPPING_ONTRAC_WEB_SERVICES_POSTAL, //OnTrac notes: 5 digit origination Zip of the package
      'DelZip' =>  $destination_postcode, //OnTrac notes: 5 digit destination Zip of the package
      'Residential' => $residential, //OnTrac notes: boolean indicating if the package destination is residential
      'COD' => '0.00', //OnTrac notes: COD value of the package
      'SaturdayDel' => MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SATURDAY, //OnTrac notes: boolean indicating if the package is for Saturday delivery
      'Declared' => (!is_null($declared_value) ? $declared_value : $order->info['total']), //OnTrac notes: Declared value of the package
      'Weight' => number_format($packed_box['weight'], 1), //OnTrac notes: Weight of the package in lbs
      'DIM' => number_format($packed_box['length'], 1) . 'X' . number_format($packed_box['width'], 1) . 'X' . number_format($packed_box['height'], 1), //OnTrac notes: Dimensions of the package in the format: (length)X(width)X(height)
      'service' => '', //OnTrac notes: * 1 character OnTrac Service code. An empty service parameter will return quotes for all available services
      'Letter' => '0' //OnTrac notes: 1 – Letter, 0 – Package
    );
  }

  function quote($method = '', $package = null) {
    /* onTrac integration starts */
   global $db, $shipping_weight, $shipping_num_boxes, $cart, $order;


    //connect to remote server
    if(MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SERVER == 'production') {
      $account_num = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_ACT_NUM;
      $password = MODULE_SHIPPING_ONTRAC_WEB_SERVICES_PASSWORD;
      $url = 'https://www.shipontrac.net/ontracwebservices/OnTracServices.svc/V2/'.$account_num.'/rates?';
    }
    else {
      $account_num = '37'; //test account and password provided by ontrac.
      $password = 'testpass';
      $url = 'https://www.shipontrac.net/ontractestwebservices/OnTracServices.svc/V2/'.$account_num.'/rates?';
    }

    //build request
    if($package == null) {
      if (MODULE_SHIPPING_BOXES_MANAGER_STATUS == 'true') {
        require_once(DIR_WS_CLASSES . 'shipping_boxes_manager.php');
        $shippingBoxesManager = new shippingBoxesManager($_SESSION['cart']->get_products(), $order);
        $shippingBoxesManager->pack_boxes();
        foreach($shippingBoxesManager->packed_boxes as $box) {
          //$box = array('width'=>, 'height'=>, 'length'=>, 'weight'=>)
          $packages[] = $this->make_package($order->delivery['postcode'], $box);  
        }
      }
      else {
        $packages[] = $this->make_package($order->delivery['postcode']);
      }
    }
    else {
      $packages[] = $package;
    }

    $request['pw'] = $password;

    foreach($packages as $package){
      if(strlen($request['packages']) > 0) $request['packages'] .= ',';
      $request['packages'] = '{' . html_entity_decode(implode(";", $package)) . '}';  
    }

    $url .= html_entity_decode(http_build_query($request));

    //Send request to remote server
  try{

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($ch);
    curl_close($ch);

    $response_xml = simplexml_load_string($response);
//    print_r($response_xml);

    $costs = array();//for sorting
    $i = 0;
    //Put output in appropriate arrays for displaying on the page

    $available_methods = explode(', ', MODULE_SHIPPING_ONTRAC_WEB_SERVICES_AVAILABLE_METHODS);

    foreach($response_xml->Shipments->Shipment->Rates->Rate as $quote){
      if(($method == '' || str_replace('_', '',  $this->_shipping_type_name((string)$quote->Service)) == $method || (string)$quote->Service == $method) && in_array($this->_shipping_type_name((string)$quote->Service), $available_methods)){
 
       $eta = '';
       if(MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SHOW_ETA == 'true'){
         $eta = '(' . (string)$quote->TransitDays . ((string)$quote->TransitDays > 1 ? ' Days' : ' Day') . ')';
       }
     
        $costs[$i] = (string)$quote->TotalCharge;
        $methods[$i] = array('id' => (string)$quote->Service,
                      'title' => (string)$this->_shipping_type_name($quote->Service) . ' ' . $eta,
                      'cost' => (string)$quote->TotalCharge);
      }
      $i++;
    }
    if(count($response_xml->Shipments->Shipment->Rates->Rate) == 0) {
      $this->quotes = array('module' => (string)$this->title,
                            'error'  => 'OnTrac does not service your zip code.');
    }
    else {
      array_multisort($costs, SORT_ASC, $methods);
      $this->quotes = array('id' => $this->code,
                          'module' => $this->title,
                          'info' => $this->info(),
                          'methods' => $methods);
    }
  } catch (SoapFault $fault) {
      $this->quotes = array('module' => (string)$this->title,
                            'error'  => 'Sorry, the OnTrac server is currently not responding, please try again later.');
  }
    return $this->quotes;
  }

  //find properties of packed box. Uses largest of each width, height, and length, and adds the weight.
  function _find_packed_box_size() {
    global $db;
    define('CM_TO_INCHES', 0.393701); //conversion number
    $products = $_SESSION['cart']->get_products();
    $packages = array('default' => 0);
    $packed_box = array('width'=>0, 'height'=>0, 'length'=>0, 'weight'=>0);
    foreach ($products as $product) {
      $dimensions_query = "SELECT products_length, products_width, products_height, products_ready_to_ship, products_dim_type FROM " . TABLE_PRODUCTS . " 
                               WHERE products_id = " . (int)$product['id'] . " 
                               AND products_length > 0 
                               AND products_width > 0
                               AND products_height > 0 
                               LIMIT 1;";
      $dimensions = $db->Execute($dimensions_query);  

     if ($dimensions->RecordCount() > 0 && $dimensions->fields['products_ready_to_ship'] == 1) {
       //convert to inches
         if($dimensions->fields['products_dim_type'] == 'cm') {
           $dimensions->fields['products_width'] = $dimensions->fields['products_width'] * CM_TO_INCHES;
           $dimensions->fields['products_height'] = $dimensions->fields['products_height'] * CM_TO_INCHES;
           $dimensions->fields['products_length'] = $dimensions->fields['products_length'] * CM_TO_INCHES;
         } 

        //find largest of each dimension
        $packed_box['width'] = ($packed_box['width'] > $dimensions->fields['products_width']) ? $packed_box['width'] : $dimensions->fields['products_width'];
        $packed_box['height'] = ($packed_box['height'] > $dimensions->fields['products_height']) ? $packed_box['height'] : $dimensions->fields['products_height'];
        $packed_box['length'] = ($packed_box['length'] > $dimensions->fields['products_length']) ? $packed_box['length'] : $dimensions->fields['products_length'];
 
       $packed_box['weight'] += $product['weight'];
      }
    }
    return $packed_box;
  }

  //helper function for making xml for soap requests
  function _generate_xml_from_array($array, $node_name) {
    $xml = '';
    if (is_array($array) || is_object($array)) {
      foreach ($array as $key=>$value) {
        if (is_numeric($key)) {
          $key = $node_name;
        }
        $xml .= '<' . $key . '>' . "" . $this->_generate_xml_from_array($value, $node_name) . '</' . $key . '>' . "";
      }
    } else {
      $xml = htmlspecialchars($array, ENT_QUOTES) . "";
    }
    return $xml;
  }

 //friendly names for the shipping types, as defined in the OnTrac documentation
  function _shipping_type_name($shipping_code) {
    switch($shipping_code) {
      case 'S':
        return 'Sunrise';
      case 'G':
        return 'Sunrise Gold';
      case 'C':
        return 'Ground';
      case 'H':
        return 'Heavyweight';
    }
   return 'Unknown shipping type';
 }

  // method added for expanded info in FEAC
  function info() {
    return MODULE_SHIPPING_ONTRAC_WEB_SERVICES_INFO; // add a description here or leave blank to disable
  }
    
  function _setInsuranceValue($order_amount){
    if ($order_amount > (float)MODULE_SHIPPING_ONTRAC_WEB_SERVICES_INSURE) {
      $this->insurance = sprintf("%01.2f", $order_amount);
    } else {
      $this->insurance = 0;
    }
  }
  
  function objectToArray($object) {
    if( !is_object( $object ) && !is_array( $object ) ) {
      return $object;
    }
    if( is_object( $object ) ) {
      $object = get_object_vars( $object );
    }
    return array_map( 'objectToArray', $object );
  }

  function check(){
    //check if module is installed
    global $db;
    if(!isset($this->_check)){
      $check_query  = $db->Execute("SELECT configuration_value FROM ". TABLE_CONFIGURATION ." WHERE configuration_key = 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_STATUS'");
      $this->check = $check_query->RecordCount();
    }
    return $this->check;
  }

  function install() {
    global $db;
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Enable OnTrac Web Services','MODULE_SHIPPING_ONTRAC_WEB_SERVICES_STATUS','true','Do you want to offer OnTrac shipping?','6','0','zen_cfg_select_option(array(\'true\',\'false\'),',now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Version Installed', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_VERSION', '2.1', '', '6', '0', now())"); 
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Server','MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SERVER','testing','Should requests be sent to OnTrac\'s testing or production server?','6','0','zen_cfg_select_option(array(\'testing\',\'production\'),',now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('OnTrac Account Number', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_ACT_NUM', '', 'Enter OnTrac account number', '6', '3', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('OnTrac Password', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_PASSWORD', '', 'Enter OnTrac account password', '6', '4', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Postal code', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_POSTAL', '', 'Enter the postal code for the ship-from street address, required', '6', '24', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Saturday Delivery', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SATURDAY', 'false', 'Enable Saturday Delivery', '6', '10', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Connection Timeout', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_CONNECTION_TIMEOUT', '15', 'Enter the maximum time limit in seconds that the server should wait when connecting to the OnTrac server.', '6', '10', now())");    
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SORT_ORDER', '0', 'Sort order of display.', '6', '999', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Shipping Info', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_INFO', '', 'Add a description that will display in Fast and Easy AJAX Checkout', '6', '99', 'zen_cfg_textarea(', now())"); 
$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Show ETA?', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SHOW_ETA', 'false', 'Show estimated date of arrival with shipping quote', '6', '10', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Available Methods', 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_AVAILABLE_METHODS', '--none--', 'Check all available shipping methods', '6', '16', 'zen_cfg_select_multioption(array(\'Sunrise\', \'Sunrise Gold\', \'Ground\', \'Heavyweight\'), ',  now())");

  }

  function remove() {
    global $db;
    $db->Execute("DELETE FROM ". TABLE_CONFIGURATION ." WHERE configuration_key in ('". implode("','",$this->keys()). "')");
  }

  function keys() {
    return array('MODULE_SHIPPING_ONTRAC_WEB_SERVICES_STATUS',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_VERSION',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SERVER', 
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_ACT_NUM',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_PASSWORD',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_POSTAL',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SATURDAY',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_CONNECTION_TIMEOUT',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_INFO',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SORT_ORDER',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SHOW_ETC',//todo remove this
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_SHOW_ETA',
                 'MODULE_SHIPPING_ONTRAC_WEB_SERVICES_AVAILABLE_METHODS'
                 );

  }
}
