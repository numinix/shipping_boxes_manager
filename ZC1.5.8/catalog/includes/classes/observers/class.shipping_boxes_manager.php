<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2007-2008 Numinix Technology http://www.numinix.com    |
// |                                                                      |
// | Portions Copyright (c) 2003-2006 Zen Cart Development Team           |
// | http://www.zen-cart.com/index.php                                    |
// |                                                                      |
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
//  $Id: class.shipping_boxes_manager.php 85 2010-04-20 00:49:04Z numinix $
//
/**
 * Observer class used to redirect to the FEC page
 *
 */
class shippingBoxesManagerObserver extends base 
{
  function shippingBoxesManagerObserver()
  {
    global $zco_notifier;
    $zco_notifier->attach($this, array('NOTIFY_SHIPPING_MODULE_CALCULATE_BOXES_AND_TARE'));
  }
  
  function update(&$class, $eventID, $paramsArray) {
    global $order, $db, $packed_boxes;
    if (MODULE_SHIPPING_BOXES_MANAGER_STATUS == 'true') {
      require_once(DIR_WS_CLASSES . 'shipping_boxes_manager.php');
      $shippingBoxesManager = new shippingBoxesManager($_SESSION['cart']->get_products(), $order);
      $shippingBoxesManager->pack_boxes();
      $GLOBALS['shipping_num_boxes'] = $shippingBoxesManager->num_boxes;
      $GLOBALS['total_weight'] = $shippingBoxesManager->new_total_weight;
      $GLOBALS['packed_boxes'] = $shippingBoxesManager->packed_boxes;
    }
  }
}
// eof
